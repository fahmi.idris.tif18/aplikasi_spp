<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\AdminController;
use App\Http\Controllers\API\SantriController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware'=>'auth:sanctum'], function()
{
    //Kelola Data Admin
    Route::post('/createAdmin', [AdminController::class, 'createAdmin']);
    Route::get('/showAdmin', [AdminController::class, 'showAdmin']);
    Route::post('/editAdmin/{id}', [AdminController::class, 'updateAdmin']);
    Route::get('/deleteAdmin/{id}', [AdminController::class, 'deleteAdmin']);
    Route::get('/logout', [AuthController::class, 'logout']);

    //Kelola Data Santri
    Route::post('/createSantri', [SantriController::class, 'createSantri']);
});

Route::post('/login', [AuthController::class, 'login']);